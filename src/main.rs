use leptos::*;
use leptos_use::storage::{use_local_storage, JsonCodec};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct Todo {
    id: usize,
    content: String,
    done: bool,
}

pub fn main() {
    mount_to_body(|| view! { <App /> })
}

#[component]
fn App() -> impl IntoView {
    view! {
        <TodoApp />
    }
}

#[component]
fn TodoApp() -> impl IntoView {
    let (todos, set_todos, _) = use_local_storage::<Vec<Todo>, JsonCodec>("todos");

    let mut next_id: usize = 0;

    let (todo, set_todo) = create_signal(String::new());

    let mut add_todo = move |content: String| {
        let new_todo = Todo {
            id: next_id,
            content,
            done: false,
        };
        next_id += 1;
        set_todos.update(|todos| todos.push(new_todo));
        set_todo.update(|todo| todo.clear());
    };

    let remove_todo = move |id: usize| {
        set_todos.update(move |todos| {
            todos.retain(move |todo| todo.id != id);
        })
    };

    let toggle_check_todo = move |id: usize| {
        set_todos.update(move |todos| {
            for todo in todos {
                if todo.id == id {
                    todo.done = !todo.done;
                }
            }
        })
    };

    view! {
        <div class="app">
            <h1>"Todo"</h1>
            <input type="text" on:input=move |e| {
                set_todo.set(event_target_value(&e));
            } prop:value=todo placeholder="Add todo" />
            <button on:click=move |_| add_todo(todo.get()) class="btn">"Add"</button>
            <For each=move || {todos.get()} key=|todo| todo.id children=move |todo| {
                view! {
                    <ul>
                        {move || todo.content.clone()}
                        <input type="checkbox" checked={move || todo.done} on:change=move |_| toggle_check_todo(todo.id) />
                        <CustomButton text="Remove" on_click=move || remove_todo(todo.id) />
                    </ul>
                }
            } />
        </div>
    }
}

#[component]
fn CustomButton(text: String, on_click: Fn() -> ()) -> impl IntoView {
    view! {
        <Button class="bg-black text-white" on:click=move |_| on_click()>{text}</Button>
    }
}
